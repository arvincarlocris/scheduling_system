<header class="main-header">
    <nav class="navbar navbar-static-top">
        <div class="container">
            <div class="navbar-header" style="padding-left:20px">
			  
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                <i class="fa fa-bars"></i>
              </button>
            </div>

            <!-- Navbar Right Menu -->
              <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                  <!-- Tasks Menu -->
                 
                 <li class="">
                    <!-- Menu Toggle Button -->
                   <a href="home.php" class="" style="font-size:14px"><i class="glyphicon glyphicon-calendar text-gray"></i>&nbsp; Class Schedule</a>
                  </li>
                  <!-- Tasks Menu -->
                  <!-- Tasks Menu -->
                 
                  <!-- Tasks Menu -->
           <li class="dropdown notifications-menu">
                    <!-- Menu toggle button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <i class="glyphicon glyphicon-print text-gray"></i> Print Schedule
                      
                    </a>
                    <ul class="dropdown-menu">
                      <li>
                        <!-- Inner Menu: contains the notifications -->
                        <ul class="menu">
                            <li><!-- start notification -->
                             <a href="#searchclass" data-target="#searchclass" data-toggle="modal" class="dropdown-toggle">
                              <i class="glyphicon glyphicon-user text-blue"></i> Classes
                            </a>
                          </li><!-- end notification -->
             
                          <li><!-- start notification -->
                             <a href="#searcht" data-target="#searcht" data-toggle="modal" class="dropdown-toggle">
                              <i class="glyphicon glyphicon-user text-blue"></i> Teacher
                            </a>
                          </li><!-- end notification -->
              
                        <li><!-- start notification -->
                             <a href="#searchroom" data-target="#searchroom" data-toggle="modal" class="dropdown-toggle">
                              <i class="glyphicon glyphicon-user text-blue"></i> Room
                            </a>
                          </li><!-- end notification -->
                          
                        </ul>
                      </li>
                     
                    </ul>
                  </li>
                  
                  <!-- Tasks Menu -->
                   <li class="dropdown notifications-menu">
                    <!-- Menu toggle button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <i class="glyphicon glyphicon-download text-gray"></i> Generate Excel
                      
                    </a>
                    <ul class="dropdown-menu">
                      <li>
                        <!-- Inner Menu: contains the notifications -->
                        <ul class="menu">
                          <li><!-- start notification -->
                             <a href="#exportclass" data-target="#exportclass" data-toggle="modal" class="dropdown-toggle">
                              <i class="glyphicon glyphicon-user text-blue"></i> Classes
                            </a>
                          </li><!-- end notification -->
             
                          
                         
                          
                        </ul>
                      </li>
                     
                    </ul>
                  </li>

                  


                  <!-- Tasks Menu -->
				   <li class="dropdown notifications-menu">
                    <!-- Menu toggle button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <i class="glyphicon glyphicon-file text-gray"></i> Entry
                      
                    </a>
                    <ul class="dropdown-menu">
                      <li>
                        <!-- Inner Menu: contains the notifications -->
                        <ul class="menu">
						  <li><!-- start notification -->
                            <a href="class.php">
                              <i class="glyphicon glyphicon-user text-blue"></i> Classes
                            </a>
                          </li><!-- end notification -->
						 
                          <li><!-- start notification -->
                            <a href="room.php">
                              <i class="glyphicon glyphicon-scale text-blue"></i> Room
                            </a>
                          </li><!-- end notification -->
						
						  <li><!-- start notification -->
                            <a href="subject.php">
                              <i class="glyphicon glyphicon-user text-blue"></i> Subject
                            </a>
                          </li><!-- end notification -->
						  
						            <li><!-- start notification -->
                            <a href="teacher.php">
                              <i class="glyphicon glyphicon-user text-blue"></i> Teacher
                            </a>
                          </li><!-- end notification -->
						              <li><!-- start notification -->
                            <a href="signatories.php">
                              <i class="glyphicon glyphicon-user text-blue"></i> Signatories
                            </a>
                          </li><!-- end notification -->
                        </ul>
                      </li>
                     
                    </ul>
                  </li>
				  <!-- Tasks Menu -->
				   <li class="dropdown notifications-menu">
                    <!-- Menu toggle button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <i class="glyphicon glyphicon-wrench text-gray"></i> Maintenance
                      
                    </a>
                    <ul class="dropdown-menu">
                      <li>
                        <!-- Inner Menu: contains the notifications -->
                        <ul class="menu">
						  
						  <li><!-- start notification -->
                            <a href="department.php">
                              <i class="glyphicon glyphicon-user text-blue"></i> Department
                            </a>
                          </li><!-- end notification -->
						  <li><!-- start notification -->
                            <a href="designation.php">
                              <i class="glyphicon glyphicon-cutlery text-blue"></i> Designation
                            </a>
                          </li><!-- end notification -->
              <li><!-- start notification -->
                            <a href="program.php">
                              <i class="glyphicon glyphicon-cutlery text-blue"></i> Program
                            </a>
                          </li><!-- end notification -->
						 
						  <li><!-- start notification -->
                            <a href="rank.php">
                              <i class="glyphicon glyphicon-send text-blue"></i> Rank
                            </a>
                          </li><!-- end notification -->
                         
						  <li><!-- start notification -->
                            <a href="salut.php">
                              <i class="glyphicon glyphicon-user text-blue"></i> Salutation
                            </a>
                          </li><!-- end notification -->
						  
						  <li><!-- start notification -->
                            <a href="sy.php">
                              <i class="glyphicon glyphicon-user text-blue"></i> School Year
                            </a>
                          </li><!-- end notification -->
						  
						  <li><!-- start notification -->
                            <a href="time.php">
                              <i class="glyphicon glyphicon-calendar text-blue"></i> Time
                            </a>
                          </li><!-- end notification -->
						
                        </ul>
                        
                      </li>
                     
                    </ul>
                  </li>
                  <!-- Tasks Menu -->

                  
					       <li class="">
                    <!-- Menu Toggle Button -->
                   <a href="settings.php" style="color:#fff;" class="dropdown-toggle">
                      <i class="glyphicon glyphicon-cog text-gray"></i>&nbsp;

                      <?php
                      include('dbcon.php');
                      $sid=$_SESSION['settings'];
                      $query=mysqli_query($con, "SELECT * FROM settings WHERE settings_id='$sid'");
                      $settings=mysqli_fetch_array($query);

                      echo $settings['sem'] . " " . "Sem" . " " . $settings['sy'];
                      ?>
                      				
                    </a>
                  </li>
                  <!-- Tasks Menu -->
				          <li class="">
                    <!-- Menu Toggle Button -->
                    <a href="profile.php" class="dropdown-toggle">
                      <i class="glyphicon glyphicon-user text-gray"></i>
                      <?php echo $_SESSION['name'];?>
                    </a>
                  </li>
                  <li class="">
                    <!-- Menu Toggle Button -->
                    <a href="logout.php" class="dropdown-toggle">
                      <i class="glyphicon glyphicon-off text-gray"></i>&nbsp;Logout 
                      
                    </a>
                  </li>
                  <script type="text/javascript" src="autosum.js"></script>
    <!-- jQuery 2.1.4 -->
    <script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
  <script src="../dist/js/jquery.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <script src="../plugins/select2/select2.full.min.js"></script>
    <!-- SlimScroll -->
    <script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../dist/js/demo.js"></script>
    <script src="../plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>
                </ul>
              </div><!-- /.navbar-custom-menu -->
          </div><!-- /.container-fluid -->

        </nav>
      </header>