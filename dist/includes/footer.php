<div id="exportclass" class="modal fade in" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content" style="height:auto">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Export as Excel File</h4>
            </div>
            <form class="form-horizontal" method="post" action="down.php" target="_blank">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label col-lg-2" for="name">Class</label>
                        <div class="col-lg-10">
                            <select class="select2" name="class" style="width:90%!important" required>
                                <?php 
                                  
                                $query2=mysqli_query($con,"select * from cys order by cys")or die(mysqli_error($con));
                                while($row=mysqli_fetch_array($query2)){
                                ?>
                                <option><?php echo $row['cys'];?></option>
                                <?php }
                                    
                                ?>
                            </select>
                        </div>
                    </div> 
                </div><hr>
                <div class="modal-footer">
                    <button type="submit" name="search" class="btn btn-success">Download</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div><!--end of modal-dialog-->
</div>
<!--end of modal--> 
<div id="searcht" class="modal fade in" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content" style="height:auto">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Search Faculty Schedule</h4>
            </div>
            <form class="form-horizontal" method="post" action="faculty_sched.php" target="_blank">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label col-lg-2" for="name">Faculty</label>
                        <div class="col-lg-10">
                            <select class="select2" name="faculty" style="width:90%!important" required>
                                <?php 
                              
                                $query2=mysqli_query($con,"select * from member order by member_last")or die(mysqli_error($con));
                                  while($row=mysqli_fetch_array($query2)){
                                ?>
                                <option value="<?php echo $row['member_id'];?>"><?php echo $row['member_last'].", ".$row['member_first'];?></option>
                                <?php }
                                
                              ?>
                            </select>
                        </div>
                    </div>               
                </div><hr>
                <div class="modal-footer">
                    <button type="submit" name="search" class="btn btn-primary">Display Schedule</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div><!--end of modal-dialog-->
</div>
<!--end of modal--> 

<div id="searchclass" class="modal fade in" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content" style="height:auto">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Search Class Schedule</h4>
            </div>
            <form class="form-horizontal" method="post" action="class_sched.php" target="_blank">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label col-lg-2" for="name">Class</label>
                        <div class="col-lg-10">
                            <select class="select2" name="class" style="width:90%!important" required>
                                <?php 
                                  
                                $query2=mysqli_query($con,"select * from cys order by cys")or die(mysqli_error($con));
                                while($row=mysqli_fetch_array($query2)){
                                ?>
                                <option><?php echo $row['cys'];?></option>
                                <?php }
                                    
                                ?>
                            </select>
                        </div>
                    </div> 
                </div><hr>
                <div class="modal-footer">
                    <button type="submit" name="search" class="btn btn-primary">Display Schedule</button>
                    <form action="down.php" method="post">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </form>
                </div>
            </form>
        </div>
    </div><!--end of modal-dialog-->
</div>
<!--end of modal--> 

<div id="searchroom" class="modal fade in" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content" style="height:auto">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Search Room Schedule</h4>
            </div>
            <form class="form-horizontal" method="post" action="room_sched.php" target="_blank">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label col-lg-2" for="name">Room</label>
                        <div class="col-lg-10">
                            <select class="select2" name="room" style="width:90%!important" required>
                                <?php 
                              
                                $query2=mysqli_query($con,"select * from room order by room")or die(mysqli_error($con));
                                  while($row=mysqli_fetch_array($query2)){
                                ?>
                                <option><?php echo $row['room'];?></option>
                                <?php }
                                
                                ?>
                            </select>
                        </div>      
                    </div>
                </div><hr>
                <div class="modal-footer">
                    <button type="submit" name="search" class="btn btn-primary">Display Schedule</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div><!--end of modal-dialog-->
</div><!--end of modal-->
<footer class="main-footer" style="text-align:center">
    <strong>Copyright &copy; 2018</strong> All rights reserved.
 <script type="text/javascript" src="autosum.js"></script>
    <!-- jQuery 2.1.4 -->
    <script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<script src="../dist/js/jquery.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <script src="../plugins/select2/select2.full.min.js"></script>
    <!-- SlimScroll -->
    <script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../dist/js/demo.js"></script>
    <script src="../plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>
</footer>